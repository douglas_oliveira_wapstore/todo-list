var inputTodo = $("#input-todo");
var todoItem = $(".todo-item").find("textarea");
var todoList = $("#todoList");
var lastSelectedTextarea = '';

var textarea = $("textarea");

var body = $("body");


$(function () {
    inputTodo.on("keydown", submitTodo);
    body.on("click", selectBody);
    body.on("keydown", removeLembrete);

    todoItem.on('dblclick',liberarCampo);
    todoItem.on("keydown", updateLembrete);
    todoItem.on("blur", updateLembrete);



    textarea.on("focus", selectTextarea);
})

/**
 * Funçao responsável por atualizar os event listeners, conforme novos itens vão sendo adicionados na todolist
 */
function updateEvents(div) {
        
    $(div).on('dblclick',liberarCampo);
    $(div).on("keydown", updateLembrete);
    $(div).on("blur", updateLembrete);

    $(div.find("textarea")).on("focus", selectTextarea);


}



/**
 * Função responsável por liberar os campos de input,quando o usuario der duplo clique
 * @param {object} e (objeto do evento)
 * @author Douglas Oliveira Paschoal
 */
 function liberarCampo(e) {
     var target = e.target;

    $(target).prop("readonly", false).trigger("focus");
}

/**
 * Método responsável por bloquear um campo de input, quando este for desfocado
 * @param {object} e (objeto do evento)
 */
 function updateLembrete(e) {
    
    if(e.which == 13)  {
        e.preventDefault();

        var mensagem = $(e.target).val();


        $.post("http://localhost/ajax/crudLembretes.php", 
        {
            comando: 'update',
            msg: mensagem,
            id: $(e.target).attr('id')
        },
            function (data) {
                var response = JSON.parse(data).response;


                if(response == 'success') {
                    
                    todoItem = $(".todo-item").find("textarea");
                     
                }

                else {
                    popupStatus("Houve um erro. Por favor, atualize a página e tente novamente.", 'error');
                    inputTodo.addClass('border-danger text-danger');
                }
            },
        )
        .fail(function () {
            popupStatus("Houve um erro. Por favor, atualize a página e tente novamente.", 'error');
        });

        todoItem.prop("readonly", true).trigger("blur");
    }

    
};

/**
 * Função responsável por submeter a mensagem no banco de dados
 * @param {object} e (evento)
 */
function submitTodo(e) {
    inputTodo.removeClass("border-danger text-danger");

    if(e.which == 13)  {

        var mensagem = inputTodo.val();

        if(mensagem == '') {
            inputTodo.addClass('border-danger text-danger');
            popupStatus("O campo não pode ser vazio", 'error');
            return;
        }

        inputTodo.prop("disabled", true);

        $.post("http://localhost/ajax/crudLembretes.php", 
        {
            comando: 'insert',
            msg: mensagem
        },
            function (data) {

                var response = JSON.parse(data);


                if(response.response == 'success') {
                    constructItem(mensagem, response.id);

                    inputTodo.val('');
                    
                    todoItem = $(".todo-item").find("textarea");       
                }

                else {
                    popupStatus("Houve um erro. Por favor, atualize a página e tente novamente.", 'error');
                    inputTodo.addClass('border-danger text-danger');
                }
            },
        ).
        always(function () {
            inputTodo.prop("disabled", false);
        })
        .fail(function () {
            popupStatus("Houve um erro. Por favor, atualize a página e tente novamente.", 'error');
            inputTodo.addClass('border-danger text-danger');
            inputTodo.prop("disabled", false);
        });

    }

}


/**
 * Função responsável por construir os blocos contendo a anotação
 * @param {string} message (mensagem que será exibida no input)
 */
function constructItem(message, id) {
    var div = $("<div class='todo-item'></div>");
    var textarea = $("<textarea class='form-control input-todo' id="+ id+ " readonly></textarea>");
    var label = $('<label for="msg"></label>');


    var dt = new Date();
    var time = dt.getDate() + '/' + (dt.getMonth() + 1) + '/' + dt.getFullYear() 
    + ' às ' + dt.getHours() +  ":" + dt.getMinutes() + ":" + dt.getSeconds();

    label.text(time);

    textarea.text(message);

    div.append(label);
    div.append(textarea);

    todoList.prepend(div);

      
    updateEvents(div);     



}


/**
 * Função responsável por escutar qual elemento está sendo clicado na tela,
 * caso seja um textarea, ele ficará na área se seleção para ser excluído
 * 
 * @param {object} e (evento)
 * @author  Douglas Oliveira Paschoal
 */

 function selectBody(e) {
     
    if(e.target.nodeName.toLowerCase() !== 'textarea') {
        $(lastSelectedTextarea).removeClass("bg-light-blue");
        lastSelectedTextarea = '';

    }
 }

 function selectTextarea(e) {
    $(lastSelectedTextarea).removeClass("bg-light-blue");

    lastSelectedTextarea = e.target;

    $(lastSelectedTextarea).addClass("bg-light-blue");
 }

 /**
  * Função responsável por remover um lembrete do banco da interface e do banco de dados
  * 
  * @param {object} e (evento)
  * @author Douglas Oliveira Paschoal
  */

 function removeLembrete(e) {
    if(e.which == 46 && lastSelectedTextarea !== '') {
        $.post("http://localhost/ajax/crudLembretes.php", 
        {
            comando: 'remove',
            id: $(lastSelectedTextarea).attr('id')
        },
            function (data) {

                var response = JSON.parse(data).response;
                if(response == 'success') {
                    $(lastSelectedTextarea).parent().slideUp(500)
                    
                    setTimeout(function () {
                        $(lastSelectedTextarea).remove()
                    }, 500);
                                  
                    
                    todoItem = $(".todo-item").find("textarea");
  
                    lastSelectedTextarea = '';  
                }

                else {
                    popupStatus("Houve um erro. Por favor, atualize a página e tente novamente.", 'error');
                    inputTodo.addClass('border-danger text-danger');
                }
            },
        )
        .fail(function () {
            popupStatus("Houve um erro. Por favor, atualize a página e tente novamente.", 'error');

        });
    }
 }
