var buttonLogin = $("#form-login-button");
var formLogin = $("#form-login");

var inputLogin = $("#email-login");
var labelInputLogin = $("#span-validate-email-login");

var passLogin = $("#pass-login");
var labelInputPass = $("#span-validate-pass-login");

var loginStatus = 'error';
var passwordStatus = 'error';

$(function () {
    formLogin.on('submit',submitLogin);
    passLogin.on('input', removeStatusMessages);
    inputLogin.on('input',removeStatusMessages);
});

/**
 * Função responsável por verificar todos os campos do form de login
 * @param {object} e 
 * @author Douglas Oliveira Paschoal
 */
function submitLogin(e) {

    e.preventDefault();
    
    var login = inputLogin;
    var password = passLogin;

    var values = [login, password];

    var anyEmpty  = verifyEmpty(values);

    if (anyEmpty) {
        popupStatus('Existem campos que não foram devidamente preenchidos.', 'error');
        return;
    }

    
        $("#spinner-entrar").removeClass('d-none');

        $.post("http://localhost/ajax/loginUser.php", 
        
        {
            action: 'login',
            email: login.val(),
            password : password.val()
        },
            
        function (data) {          

            var object = JSON.parse(data);

                if (object.response != 'valid') {
                    popupStatus('Usuário ou senha estão incorretos.', 'error');
                    return;
                }

                window.location.href = object.redirect;

            }
        )
        .always(function () {
            $("#spinner-entrar").addClass('d-none');
            buttonLogin.text('Entrar');
        }
        )
        .fail(function () {
            popupStatus('Houve um erro. Por favor, atualize a página e tente novamente.', 'error');
        });
    

}


/**
 * Função responsável por remover as mensagens de alerta do sistema
 */

 function removeStatusMessages() {
     $(this).removeClass('border-success text-success');
     $(this).removeClass('border-danger text-danger');
     $(this).parent().find('span').text('');
 }