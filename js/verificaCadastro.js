var nome = $("#nome-cadastro");
var textSpanNome = $("#span-validate-name");

var email = $('#email-cadastro');
var textSpanEmail = $('#span-validate-email');

var password = $("#senha-cadastro");
var textSpanPass = $("#span-validate-password");

var passwordMirror = $("#senha-espelho-cadastro");
var textSpanPassMirror = $("#span-validate-password-match");

var buttonCadastrar = $("#button-cadastrar");

var statusEmail = 'error';
var statusPassword = 'error';
var statusMirror = 'error';

var formCadastro = $("#form-cadastro");



var spinner = $('#spinner-loading');

$(function () {
    
    email.on('input', validateEmail);
    password.on('input', validatePassword);
    passwordMirror.on('input', validatePasswordMatch);
    buttonCadastrar.on('click', submitForm);
    nome.on('input', removeStatusMessages);

});

//FUNÇÃO RESPONSÁVEL POR VALIDAR OS EMAILS PASSADOS NO INPUT
function validateEmail() {

    spinner.removeClass('d-none');

    $.post("http://localhost/ajax/verifyEmail.php", 
    {
        email: email.val()
    },
    verifyEmailInputs
    )
    
    .fail(function () {

        email.removeClass('border-success text-success');
        email.addClass('border-danger text-danger');
        textSpanEmail.text('Ocorreu um erro. Por favor, atualize a página e tente novamente.');
    })
    
    .always(function () {
        spinner.addClass('d-none');
    });

}

//FUNÇÃO RESPONSÁVEL POR ESTILIZAR OS INPUTS, DE ACORDO COM A RESPOSTA DO REQUEST
function verifyEmailInputs(data) {
        var response = JSON.parse(data).response;


        if(response == 'valid') {
                email.addClass('border-success text-success');
                email.removeClass('border-danger text-danger');
                textSpanEmail.text('');
                statusEmail = 'success';
                
        }
        else {
                email.removeClass('border-success text-success');
                email.addClass('border-danger text-danger');
                statusEmail = 'error';

                if(response == 'invalid') textSpanEmail.text('O email é inválido.');
                if (response ==='exists') textSpanEmail.text('O email já está em uso.');
                if(response == 'empty') textSpanEmail.text('Este campo não pode ser vazio.');

        }
}

//FUNÇÃO RESPONSÁVEL POR VERIFICAR AS SENHAS

function validatePassword() {

    var length = password.val().length;
    
    if (length >= 8) {
        password.addClass('border-success text-success');
        password.removeClass('border-danger text-danger');
        textSpanPass.text('');
        statusPassword = 'success';
    }
    else {
        password.removeClass('border-success text-success');
        password.addClass('border-danger text-danger');
        statusPassword = 'error';

        if(length > 0) {
            textSpanPass.text('A senha deve possuir 8 caracteres ou mais.');
        }

        else {
            textSpanPass.text('Este campo não pode ser vazio.');
        }
    }

    validatePasswordMatch();

}


//FUNÇÃO RESPONSÁVEL POR VERIFICAR SE AMBOS OS CAMPOS DE SENHA SE EQUIVALEM
function validatePasswordMatch() {

    var length = passwordMirror.val().length;

    
    if (length == 0) { 
        passwordMirror.removeClass('border-success text-success');
        passwordMirror.addClass('border-danger text-danger');
        textSpanPassMirror.text('Este campo não pode ser vazio.'); statusMirror = 'error'; 
    }

    else if (password.val() == passwordMirror.val()) {
        passwordMirror.addClass('border-success text-success');
        passwordMirror.removeClass('border-danger text-danger');
        textSpanPassMirror.text('');
        statusMirror = 'success';
    }

    else {
        passwordMirror.removeClass('border-success text-success');
        passwordMirror.addClass('border-danger text-danger');
        statusMirror = 'error';
        
        textSpanPassMirror.text('As senhas não se equivalem.');
    }

}


//FUNÇÃO RESPONSÁVEL POR VERIFICAR SE TODOS OS INPUTS FORAM ESCRITOS CORRETAMENTE ANTES DE REALIZAR O SUBMIT
function submitForm(e) {
    
    e.preventDefault();

    if(statusEmail == 'error'|| statusPassword == 'error' || statusMirror == 'error') {
        popupStatus('Existem campos que não foram devidamente preenchidos!', 'error');

        var values = [nome, email, password, passwordMirror];

        verifyEmpty(values);

    }

    else {
        $.post("http://localhost/ajax/loginUser.php", 
        
        {
            action: 'cadastrar',
            nome: nome.val(),
            email: email.val(),
            senha : password.val()
        },
            
        function (data) {          

            var object = JSON.parse(data);


                window.location.href = object.redirect;

            }
        )
        .always(function () {
            $("#spinner-entrar").addClass('d-none');
            buttonLogin.text('Entrar');
        }
        )
        .fail(function () {
            popupStatus('Houve um erro. Por favor, atualize a página e tente novamente.', 'error');
        });
    
    }
}


/**FUNÇÃO RESPONSÁVEL POR VERIFICAR SE DETERMINADOS CAMPOS ESTÃO VAZIOS 
* @var array values (valores a serem verificados)
* @return boolean 
**/
function verifyEmpty(values) {
    var retorno = false;

    values.forEach(function (item) {

        if (item.val() == '') {
            item.removeClass('border-success text-success');
            item.addClass('border-danger text-danger');

            item.parent().find('span').text('Este campo não pode ser vazio!');

            retorno = true;

        }
    });

    return retorno;
}