
/**FUNÇÃO RESPONSÁVEL POR MOSTRAR AS MENSAGENS DOS POPUPS
 * @var {string} msg       (a mensagem que será exibida no popup)
 * @var {string} status    (tipo da mensagem [error, success])
 * 
 **/

function popupStatus(msg, status) {

    
    var modal = $("#modal-" + status);
    
    modal
    .animate({
        opacity: 'hide',
        top: '10px'
    }, 0)
    
    .animate({
        opacity: 'show',
        top: '+=15px'
    }, 500, 'swing');
    
    var timeout = setTimeout(() => {
        modal.animate({
            opacity: 'hide',
            top: '10px'
        }, 500);

    }, 5000);
    
    modal.find('p').text(msg);

    modal.click(function () { 
        $(this).stop();

        $(this).animate({
            opacity: 'hide',
            top: '-=15px'
        }, 500);

        clearTimeout(timeout);
    });
    
}
