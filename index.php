<?php

require __DIR__.'/vendor/autoload.php';

include __DIR__.'/config/routes.php';


use App\MVC\Controller;


$route = $_GET['route'] ?? null;


//array de rotas: config/routes.php

$obController = new Controller();

if(!array_key_exists($route, $routes)) {
    http_response_code(404);
    exit;
}

$page = $routes[$route];



$obController->$page();




