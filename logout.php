<?php 

require  __DIR__.'/vendor/autoload.php';

use App\Session\Login;

if(isset($_GET['acao']) && $_GET['acao'] == 'logout') {
    Login::logout();
}