<?php 

namespace App\Entity;

use App\Db\Database;
use App\Entity\Usuario;
use App\Session\Login;
use PDO;

/**
 * Classe responsável por gerenciar os lembretes associados a cada usuário
 * 
 * @author Douglas Oliveira Paschoal
 */

 class Lembrete {

    /**
     * Identificador do lembrete
     * @var integer
     */
    public $id;


    /**
     * Mensagem do lembrete
     * @var string
     */

     public $msg;

     /**
      * Data e hora que o lembrete foi inserido
      * @var string
      */

      public $date_lembrete;

       /**
     * Função responsável por realizar um lembrete do usuário no sistema
     * @return boolean
     */
    
     public function cadastrar() {

        $obDatabase = new Database('lembrete');
        
        $obUsuario = Usuario::getUserByEmail(Login::getUsuarioLogado()['email']);

        date_default_timezone_set('America/Sao_Paulo');
        $date = date('Y-m-d H:i:s');

        $this->id = $obDatabase->insert([
            'msg' => $this->msg,
            'id_usuario' => $obUsuario->id,
            'date_lembrete' => $date
        ]);

        return $this->id;


     }

/**
 * Função responsável por alterar os dados de lembretes do banco de dados
 * 
 * @param array $values 
 * @param string $where
 * @return boolean
 */
     public function alterar($id) {

        $obDatabase = new Database("lembrete");

        $obDatabase->update([
            'msg' => $this->msg
        ], "id = ". $id);

        return true;

     }


/**
 * Função responsável pro obter determinado lembrete do banco baseando-se em seu ID
 * @var string $msg
 */
     public static function getLembreteById($id) {

        return (new Database('lembrete'))->select('id ='.$id)->fetchObject(self::class);

     }


    /**
     * Função responsável por remover determinados registros de lembretes do banco de dados
     * @param integer id
     */

     public static function remover($id) {

        $obUserTodo = new Database('usuario_lembrete');
        
         return (new Database('lembrete'))->delete('id ='.$id);
     }


     /**
      * Função responsável por obter todos os lembretes do banco de dados
      * @param integer id_user
      */

      public static function getLembretes($id_user) {
          return (new Database('lembrete'))->select('id_usuario ='.$id_user,'id desc')->fetchAll(PDO::FETCH_CLASS, self::class);
      }


 }




?>