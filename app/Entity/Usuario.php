<?php 

namespace App\Entity;

use App\Db\Database;
use PDO;

/**
 * Classe responsável por gerenciar os usuários cadastrados nos sistema
 * 
 * @author Douglas Oliveira Paschoal
 */

class Usuario  
{
    
    /**
     * Identificador do usuário
     * @var integer
     */

     public $id;

     /**
      * Nome do usuário
      * @var string
      */

    public $nome;

    /**
     * Email do usuário
     * @var string
     */
    public $email;

    /**
     * Senha do usuário
     * @var string
     */
    public $senha;

   /**
     * Função responsável por realizar o cadastro do usuário no sistema
     * @return boolean
     */
    
     public function cadastrar() {

        $obDatabase = new Database('usuario');

        $this->id = $obDatabase->insert([
            'nome' => $this->nome,
            'email' => $this->email,
            'senha' => $this->senha
        ]) ;

        return true;


     }


     /**
      * Função responsável por obter todos os emails do banco de dados
      * @return PDO
      */

      public static function getEmails($where = null) {
        return (new Database('usuario'))->select('email = "'.$where.'"', null, null, 'email')->fetchAll(PDO::FETCH_ASSOC);
      }

      /**
       * Função responsável por buscar um usuário no banco com base no email
       * @var string email
       * @return PDO
       */
      public static function getUserByEmail($email) {
        return (new Database('usuario'))->select('email = "'. $email .'"')->fetchObject(self::class);
      }



}



?>