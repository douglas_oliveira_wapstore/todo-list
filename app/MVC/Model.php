<?php 


namespace App\MVC;

use App\Session\Login;
use App\Entity\Lembrete;

/**
 * Classe responsável por gerenciar as regras de negócio, fazer consultas de dados
 * para exibi-los na inteface do usuário
 */
class Model {

    /**
     * Função responsável por obter a sessão do usuário
     */
    public function getUserSession() {
        if(Login::isLogged()) {
            return Login::getUsuarioLogado();
        }

        else return null;
    }

    /**Função responsável por obter o primeiro nome do usuario
     */
    public function getUserFirstName() {
        $name = $this->getUserSession()['nome'];

        $array = explode(' ', $name);

        return $array[0];
    }

    /**
     * Função responsável por obter todos os lembretes do banco de dados
     * @return string
     */

     public function getLembretesHTML() {

        $lembretes = Lembrete::getLembretes($this->getUserSession()['id']);

        $listagemLembretes = '';

        foreach ($lembretes as $key => $lembrete) {
            $listagemLembretes .= '
            <div class="todo-item">
                <label for="msg">'.date('d/m/Y à\s H:i:s', strtotime($lembrete->date_lembrete) ) .'</label>
                <textarea class="form-control input-todo" id="'.$lembrete->id.'" name="msg" readonly>'.html_entity_decode($lembrete->msg).'</textarea>

            </div>';
        }

        return $listagemLembretes;

     }

}