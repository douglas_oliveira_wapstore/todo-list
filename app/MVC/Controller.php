<?php 


namespace App\MVC;

use App\MVC\Model;
use App\MVC\View;
use App\Session\Login;



/**
 * Classe responsável por gerenciar as rotas passadas pela URL,
 * redirecionando para suas respectivas views, gerenciado e organizando os templates e 
 * os dados que serão exibidos na interface.
 * 
 * @author Douglas Oliveira Paschoal
 */
class Controller {

    /**
     * Variável que abriga todos os dados que serão passados para a página
     * 
     * @var array
     */
    public $data = [];

    /**
     * Construtor que faz as devidas verificações prévias dos dados a serem exibidos
     */

    public function __construct() {

        $model = new Model();

        $userLogged = $model->getUserSession();

        $this->data['nav'] = '';

        if(!is_null($userLogged)) {
            $name = $userLogged['nome'];
            $array = explode(' ', $name);

            $this->data['msg'] = 'Olá,'.$array[0];
            $this->data['navItem'] = '<a href="/todo"> Minha lista </a>';
            $this->data['msgLogin'] = '<a href="?acao=logout" class="account-{{class}}"> Sair </a>';

        }
        else {
            $this->data['msg'] = 'Olá, visitante.';
            $this->data['msgLogin'] = '<a href="/loggin" class="account-{{class}}"> Entrar </a>';
            $this->data['navItem'] = '';
        }

        include __DIR__.'/../../logout.php';
    }

    /**
     * Função que exibe e organiza a home do usuário 
     */
    public function home() {
        $model = new Model();
        $view = new View();

        $this->data['class'] = 'soft';
        $this->data['nav'] = '<nav class="background-static background-nav">';
        
        $view->paths = [
            'header' => View::PATTERN_PATH . 'header.php',
            'content' => View::PATTERN_PATH . 'home.php',
            'footer'  => View::PATTERN_PATH . 'footer.php'
        ];

        $pages = $view->render($view->paths, $this->data);

        $view->draw($pages);
    }

    /**
     * Função que exibe a página de login
     */

     public function login() {
        $model = new Model();
        $view = new View();

        $this->data['class'] = 'bold';

        Login::requireLogout();

        $view->paths = [
            'header' => View::PATTERN_PATH . 'header.php',
            'content' => View::PATTERN_PATH . 'form-login.php',
            'footer' => View::PATTERN_PATH . 'footer.php'
        ];

        $pages = $view->render($view->paths, $this->data);

        $view->draw($pages);

     }



     /**
      * Função que exibe a página dos todos
      */

    public function todo() {
        $model = new Model();
        $view = new View();

        // INFORMAÇÕES DE LOGIN

        Login::requireLogin();
        $this->data['class'] = 'bold';

        $userLogged = $model->getUserSession();
        $userName = $model->getUserFirstName();

        $this->data['lembretes'] = $model->getLembretesHTML($userLogged['id']);

        $view->paths = [
            'header' => View::PATTERN_PATH . 'header.php',
            'content' => View::PATTERN_PATH. 'todos.php',
            'footer'=> View::PATTERN_PATH . 'footer.php'
        ]; 



        $pages = $view->render($view->paths, $this->data);


        $view->draw($pages);

    }


}


?>