<?php

namespace App\MVC;


/**
 * Classe responsável por obter todas as views do site, bem como respostas de requisições,
 * dentre outros
 * 
 * @author Douglas Oliveira Paschoal
 */
class View {


    /**
     * Caminho padrão dos arquivos de view
     */
    const PATTERN_PATH = 'http://localhost/includes/';

    /**
     * Variável que abriga todas as views que serão exibidas na página
     * 
     * @var array
     */

    public $paths = [];

    /**
     * Função responsável por renderizar a página, extrair os dados do controller e mesclá-los, com as views
     * 
     * @param array $paths
     * @param array $values
     * 
     * @return array
     */
    public function render($paths, $values = []) {
        $pages = $paths;

        foreach($paths as $title => $path) {
            $file = file_get_contents($path);

            $pages[$title] = $file;

                foreach($values as $key => $value) {
                    $pages[$title] = str_replace('{{'.$key.'}}', $value, $pages[$title]);
                }
        }

        return $pages;

    }

    /**
     * Metodo responsável por incluir os arquivos e exibi-los na página
     * 
     * @param array $pages
     * 
     */
    public function draw($pages = []) {

        foreach($pages as $page) {

            echo $page;
        }

    }

}