<?php 


namespace App\Session;

class Login {

    /**
     * Método responsável por iniciar a sessão
     */

    private static function init() {
        //VERIFICA STATUS DA SESSÃO
        if(session_status() !== PHP_SESSION_ACTIVE) {
            //INICIA SESSÃO
            session_start();
        }
    }

    /**
     * Metodo responsável por criar sessão do usuário
     * @param Usuario
     */
    public static function login($obUsuario) {
        //INICIA A SESSÃO
        self::init();

        $_SESSION['usuario'] = [
            'id' => $obUsuario->id,
            'nome' => $obUsuario->nome,
            'email' => $obUsuario->email
        ];
        //session é uma variável global que armazena dados entre páginas
        //SESSION temos acesso a toda a sessão
        //é do tipo array, que não se recomendam se salvar objetos
    }

    /**
     * Método responsável por verificar se o usuario está logado
     * @return boolean
     */

    public static function isLogged() {
        self::init();

        return isset($_SESSION['usuario']['id']);
    }

    /**
     * Método responsável por obrigar o usuário a estar logado para acessar
     */

    public static function requireLogin() {
        if(!self::isLogged()) {
            header('location: /loggin');
            exit;
        }
    }
        /**
     * Método responsável por obrigar o usuário a estar deslogado para acessar
     */

    public static function requireLogout() {
        if(self::isLogged()) {
            header('location: /todo');
            exit;
        }
    }

    /**
     * Método responsável por deslogar usuário
     */
    public static function logout(){
        //INICIA A SESSÃO
        self::init();

        //REMOVE A SESSÃO DE USUÁRIO
        unset($_SESSION['usuario']);

        //REDIRECIONA O USUÁRIO PARA LOGIN
        header('location: /loggin');
        exit;
    }

    /**
     * Método responsável por retornar os dados do usuário logado
     * @return array
     */
    public static function getUsuarioLogado() {
        self::init();

        return self::isLogged() ? $_SESSION['usuario'] : null;

    }


}
