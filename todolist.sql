create database todolist;

use todolist;

create table usuario(
	id integer not null auto_increment primary key,
    nome varchar(255) not null,
    email varchar(255) not null,
    senha varchar(255) not null
);

create table usuario_lembrete(
	id_usuario integer not null,
    id_lembrete integer not null,
    foreign key(id_usuario) references usuario(id),
    foreign key(id_lembrete) references lembrete(id)
);

create table lembrete(
	id integer not null auto_increment primary key,
    msg varchar(255) not null
);

select * from usuario;