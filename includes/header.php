<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Treinando Jquery</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="css/nav.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/footer.css">




</head>

<body class="bg-light-blue-body">
   {{nav}} 
    <!-- MENSAGENS DE STATUS EMITIDOS PELO SISTEMA -->
        <div class="modal-success alert alert-success" id="modal-success">
            <p></p>
        </div>
        <div class="modal-error alert alert-danger" id="modal-error">
            <p></p>
        </div>

        <!-- -------------------------------------------- -->
        <div class="nav-menu-{{class}}">
                <a href="/home"><li>Home</li></a>
                {{navItem}}
                {{msgLogin}}
                <a href="#" class="account-{{class}}">{{msg}} <i class="fas fa-user-circle"></i></a>
                <a href="#">Getting started</a>
                <a href="#">Contate-nos</a>
        </div>