
<div class="main-title">
    <h1>Todo list</h1>
    <h2>A organização é uma virtude.</h2>
</div>
</nav>

<br>
<main>
    
    <div class="citacao">
        <h1>"Com organização e tempo, acha-se o segredo de fazer tudo e bem feito."</h1>
        
        
        
        <div class="text-home">
            <p>Todolist é uma solução completa para você que quer recuperar a tranquilidade e o controle.</p>
            <p>Venha fazer parte do site de produtividade mais bem avaliado do mercado.</p>
        </div>
        
    </div>
    
    <div class="background-home1 background-static">
        <div class="div1-background-home1 div-bg">
            <p class="p-background-home1">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <br>
                "Sempre usei o todolist para organizar as tarefas da minha empresa"
            </p>
        </div>
        
        <div class="div2-background-home1 div-bg">
            <p class="p2-background-home1">
            <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <br>

                "Não me arrependo, é a melhor ferramenta de produtividade do momento."
            </p>
        </div>
        
        
        <div class="div3-background-home1 div-bg">
            <p class="p3-background-home1">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <br>

                "Simples e eficiente. A melhor que já usei, sem dúvida."
            </p>
        </div>

    </div>
    
    <div class="graph">
        <h1>Avaliação nos maiores sites de aplicativos</h1>
        <p>2020, por Reclame Aqui</p>
        <img src="https://i.ibb.co/87y4xCV/graph.png" alt="">
    </div>

    <div class="background-static background-home3">
        <div class="text-home2">
            <p>Se junte ao grupo de</p>
            <h1>1.500.000</h1>
            <p>de usuários que utilizam o todolist.</p>

            <a href="/loggin"><button class="btn btn-outline-primary btn-start">Vamos começar!</button></a>
        </div>
    </div>

</main>
