

    <div class="container p-4 bg-light mt-3">

        <div class="row">

            <div class="col center p-3">

                <h1 class="title-login">Entrar</h1>

                <br>

                <form class="form" name="login" id="form-login">

                    <div class="form-group">
                        <label for="login">Login:</label>
                        <input type="text" class="form-control" name="login" id="email-login" required>
                        <span id="span-validate-email-login" class="text-danger"></span>
                    </div>

                    <div class="form-group">

                        <label for="senha">Senha:</label>
                        <input type="password" class="form-control" name="senha" id="pass-login" required>
                        <span id="span-validate-pass-login" class="text-danger"></span>

                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" name="enviar" value="login" id="form-login-button">
                            Entrar
                            <div class="spinner-border spinner-border-sm text-light d-none" role="status" id="spinner-entrar"></div>
                        </button>
                    </div>

                </form>

            </div>

            <div class="col p-3">
                <form method="post" action="/../login.php" name="cadastro" id="form-cadastro">
                    <h1 class="title-login">Cadastre-se</h1>

                    <div class="form-group">

                        <label for="nome">Nome:</label>
                        <input type="text" class="form-control" name="nome" id="nome-cadastro" required>
                        <span id="span-validate-name" class="text-danger"></span>

                    </div>

                    <div class="form-group">

                        <label for="email">Email:</label>
                        <div class="spinner-border spinner-border-sm text-primary d-none" role="status" id="spinner-loading">
                        </div>
                        <input type="email" class="form-control" name="email" id="email-cadastro" required>
                        <span id="span-validate-email" class="text-danger"></span>

                    </div>

                    <div class="form-group">

                        <label for="senha">Senha:</label>
                        <input type="password" class="form-control" name="senha" id="senha-cadastro" required>
                        <span id="span-validate-password" class="text-danger"></span>


                    </div>


                    <div class="form-group">

                        <label for="senha">Confirme sua senha:</label>
                        <input type="password" class="form-control" id="senha-espelho-cadastro" required>
                        <span id="span-validate-password-match" class="text-danger"></span>

                    </div>


                    <div class="form-group">

                        <button class="btn btn-primary" name="enviar" value="cadastro" id="button-cadastrar">Cadastrar</button>

                    </div>

                </form>

            </div>

        </div>

    </div>


