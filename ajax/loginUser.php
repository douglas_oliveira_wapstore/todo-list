<?php

 require __DIR__.'./../vendor/autoload.php';

 use App\Entity\Usuario;
 use App\Session\Login;

$response = [
    'response' => 'invalid'
];


if(isset($_POST['action'])&&$_POST['action'] == 'login') {

    $obUser = Usuario::getUserByEmail($_POST['email']);

    if(!$obUser instanceof Usuario) {
        echo json_encode($response);
        exit;
    }

    if(password_verify($_POST['password'], $obUser->senha)) {
        $response['response'] = 'valid';

            Login::login($obUser);

            $response['redirect'] = 'http://localhost/todo'; 

            echo json_encode($response);
            exit;
    }

    echo json_encode($response);
    exit;

}


if(isset($_POST['action'])&&$_POST['action']=='cadastrar') {

    if(isset($_POST['nome'], $_POST['email'], $_POST['senha'])) {
        $obUser = new Usuario();

        $obUser->nome = $_POST['nome'];
        $obUser->email = $_POST['email'];
        $obUser->senha = password_hash($_POST['senha'], PASSWORD_DEFAULT);

        $success = $obUser->cadastrar();

        Login::login($obUser);

        $response['response'] = 'valid';
        $response['redirect'] = 'http://localhost/todo'; 
    }
}

echo json_encode($response);
exit;