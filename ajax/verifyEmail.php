<?php

 require __DIR__.'/../vendor/autoload.php';

 use App\Entity\Usuario;

if(isset($_POST['email'])) {


    $obUsers = Usuario::getEmails($_POST['email']);
    $isEmailValid = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);

    $response = ['response' => 'valid'];

    if(count($obUsers) > 0) $response['response'] = 'exists';
    if($isEmailValid === false) $response['response'] = 'invalid';
    if(strlen($_POST['email']) == 0) $response['response'] = 'empty';

    echo json_encode($response);

}








?>