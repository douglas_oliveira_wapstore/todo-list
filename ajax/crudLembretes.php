<?php

 require __DIR__.'./../vendor/autoload.php';


 use App\Entity\Lembrete;


if(isset($_POST['comando'])) {

    $response = [
        'response' => 'error'
    ];

    switch($_POST['comando']) {
        case 'insert':

            if(isset($_POST['msg'])) {
            
                $obLembrete = new Lembrete();

                $msg = filter_input(INPUT_POST, 'msg', FILTER_SANITIZE_STRING);

                $obLembrete->msg = htmlentities($msg);

                $id = $obLembrete->cadastrar();

                $response['response'] = 'success';
                $response['id'] = $id;

            }

            else $response['response'] = 'error';

        break;

        case 'update': 


            if(isset($_POST['msg'], $_POST['id'])) {

                $obLembrete = new Lembrete();

                $obLembrete->msg = filter_input(INPUT_POST, 'msg', FILTER_SANITIZE_STRING);

                $obLembrete->alterar($_POST['id']);


                $response['response'] = 'success';

            }

            else $response['response'] = 'error';

        break;

        case 'remove':

            if(isset($_POST['id']) && is_numeric($_POST['id'])) {
                $lembrete = Lembrete::getLembreteById($_POST['id']);

                Lembrete::remover($lembrete->id);


                $response['response'] = 'success';

            }

            else $response['response'] = 'error';

            break;
        
        }
    
    echo json_encode($response, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

}
