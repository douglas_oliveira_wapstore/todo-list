# Todo List

O desenvolvimento deste site é parte componente de um dos instrumentos de avaliação para iniciação de atividades na empresa "web.art group". Constitui, basicamente, em uma todo-list 100% personalizável e com conexão com o banco de dados.

## Getting started

* Primeiramente, execute todos os comandos SQL presentes no arquivo [todolist.sql](https://gitlab.com/douglas_oliveira_wapstore/todo-list/-/blob/main/todolist.sql).

* Logo após, abra um terminal no local do projeto instalado e execute o comando *"composer install"* para a importação de dependências.

* Por fim, aproveite!
